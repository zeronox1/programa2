-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema FACTURACION
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema FACTURACION
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `FACTURACION` DEFAULT CHARACTER SET utf8 ;
USE `FACTURACION` ;

-- -----------------------------------------------------
-- Table `FACTURACION`.`Admin`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `FACTURACION`.`Admin` (
  `Admin` VARCHAR(15) NOT NULL,
  `Contraseña VARCHAR(15)` VARCHAR(45) BINARY NULL,
  PRIMARY KEY (`Admin`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `FACTURACION`.`Factura`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `FACTURACION`.`Factura` (
  `ID_Factura` INT NOT NULL AUTO_INCREMENT,
  `Cantidad` SMALLINT NULL,
  `Fecha y hora` DATETIME NULL,
  `Mesa` TINYINT NULL,
  `Admin` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`ID_Factura`, `Admin`),
  UNIQUE INDEX `ID_Factura_UNIQUE` (`ID_Factura` ASC) VISIBLE,
  INDEX `fk_Factura_Admin1_idx` (`Admin` ASC) VISIBLE,
  CONSTRAINT `fk_Factura_Admin1`
    FOREIGN KEY (`Admin`)
    REFERENCES `FACTURACION`.`Admin` (`Admin`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `FACTURACION`.`CLIENTES`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `FACTURACION`.`CLIENTES` (
  `# de cedula cliente` INT NOT NULL,
  `Telefono_cliente` BIGINT(11) NOT NULL,
  `Direccion_cliente` VARCHAR(32) NOT NULL,
  `Nombre_cliente` VARCHAR(45) NOT NULL,
  `ID_cliente` INT NOT NULL,
  `Admin` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`# de cedula cliente`, `ID_cliente`, `Admin`),
  UNIQUE INDEX `#cedula de cliente_UNIQUE` (`# de cedula cliente` ASC) VISIBLE,
  INDEX `fk_CLIENTES_Factura1_idx` (`ID_cliente` ASC, `Admin` ASC) VISIBLE,
  CONSTRAINT `fk_CLIENTES_Factura1`
    FOREIGN KEY (`ID_cliente` , `Admin`)
    REFERENCES `FACTURACION`.`Factura` (`ID_Factura` , `Admin`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `FACTURACION`.`EMPLEADOS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `FACTURACION`.`EMPLEADOS` (
  `Nombre de empleado` VARCHAR(16) NOT NULL,
  `# de cedula empleado` INT NOT NULL,
  `Telefono_empleado` BIGINT(11) NOT NULL,
  `ID_empleado` INT NOT NULL,
  `Admin` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`# de cedula empleado`, `ID_empleado`, `Admin`),
  UNIQUE INDEX `# de cedula empleado_UNIQUE` (`# de cedula empleado` ASC) VISIBLE,
  INDEX `fk_EMPLEADOS_Factura1_idx` (`ID_empleado` ASC, `Admin` ASC) VISIBLE,
  CONSTRAINT `fk_EMPLEADOS_Factura1`
    FOREIGN KEY (`ID_empleado` , `Admin`)
    REFERENCES `FACTURACION`.`Factura` (`ID_Factura` , `Admin`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `FACTURACION`.`Metodo de pago`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `FACTURACION`.`Metodo de pago` (
  `# de comprobante` INT NOT NULL AUTO_INCREMENT,
  `Tarjeta` VARCHAR(5) NULL DEFAULT 'No',
  `Nequi` VARCHAR(5) NULL DEFAULT 'No',
  `Daviplata` VARCHAR(5) NULL DEFAULT 'No',
  `ID_pago` INT NOT NULL,
  `Admin` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`# de comprobante`, `ID_pago`, `Admin`),
  UNIQUE INDEX `# de comprobante_UNIQUE` (`# de comprobante` ASC) VISIBLE,
  INDEX `fk_Metodo de pago_Factura1_idx` (`ID_pago` ASC, `Admin` ASC) VISIBLE,
  CONSTRAINT `fk_Metodo de pago_Factura1`
    FOREIGN KEY (`ID_pago` , `Admin`)
    REFERENCES `FACTURACION`.`Factura` (`ID_Factura` , `Admin`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `FACTURACION`.`PRODUCTO`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `FACTURACION`.`PRODUCTO` (
  `Nombre producto` VARCHAR(50) NOT NULL,
  `Descripcion producto` VARCHAR(255) NULL,
  `Valor producto` INT NOT NULL,
  `ID_producto` INT NOT NULL,
  `Admin` VARCHAR(15) NOT NULL,
  UNIQUE INDEX `ID_Producto_UNIQUE` (`Nombre producto` ASC) VISIBLE,
  PRIMARY KEY (`ID_producto`, `Admin`),
  INDEX `fk_PRODUCTO_Factura1_idx` (`ID_producto` ASC, `Admin` ASC) VISIBLE,
  CONSTRAINT `fk_PRODUCTO_Factura1`
    FOREIGN KEY (`ID_producto` , `Admin`)
    REFERENCES `FACTURACION`.`Factura` (`ID_Factura` , `Admin`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `FACTURACION`.`ORDENES`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `FACTURACION`.`ORDENES` (
  `# de orden` INT NOT NULL AUTO_INCREMENT,
  `ID_orden` INT NOT NULL,
  `Admin` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`# de orden`, `ID_orden`, `Admin`),
  INDEX `fk_ORDENES_Factura1_idx` (`ID_orden` ASC, `Admin` ASC) VISIBLE,
  CONSTRAINT `fk_ORDENES_Factura1`
    FOREIGN KEY (`ID_orden` , `Admin`)
    REFERENCES `FACTURACION`.`Factura` (`ID_Factura` , `Admin`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
